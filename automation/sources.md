# DNS Automation Sources

* [octodns\/octodns\: Tools for managing DNS across multiple providers](https://github.com/octodns/octodns#providers "octodns/octodns: Tools for managing DNS across multiple providers")
* [AnalogJ\/lexicon\: Manipulate DNS records on various DNS providers in a standardized way\.](https://github.com/AnalogJ/lexicon "AnalogJ/lexicon: Manipulate DNS records on various DNS providers in a standardized way.")
